const app = require('express')();
const http = require('http').createServer(app);
const io = require('socket.io')(http);

// io.on('connection', socket => {
//   console.log('a user connected');

//   socket.emit('socket', 'SocketIo')

//   socket.on('message', data => {
//     console.log('message ->', data);

//     io.emit('message', data);
//   })
// });


io.on('connection', socket => {
  console.log('a user connected');

  socket.emit('socket', 'SocketIo');

  socket.on('message', data => {
    socket.join(data);
    io.to(data).emit(data, `only VIP persons can see it ${data}`);
    io.emit('message', data);
  })
});

// io.of('/user').on('connection', socket => {
//   console.log('user connect in /user/');

//   socket.on('message', data => {
//     console.log('message user ->', data);

//     io.of('/user').emit('message', data);
//   })
// })


http.listen(8080, () => { console.log('SocketIO - ON -> listening *: 8080') });


