const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8080 }, () => console.log('WebSocket - ON -> listening *: 8080'));

wss.on('connection', ws => {
    console.log('user connected!')
    ws.on('message', message => {
        console.log('message', message);
        wss.clients.forEach(client => {
            //условия кому отправлять, кому нет    
            client.send(JSON.stringify({ data: message }))
        })
        // setTimeout(() => wss.close(), 2000)
    });

    ws.send(JSON.stringify({ type: 'title', data: 'WebSocket' }));
});
