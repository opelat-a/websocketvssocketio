import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './webSocket';
import App from './socketIo';

ReactDOM.render(<App />, document.getElementById('root'));


