import React, { useEffect, useState } from 'react';
import './App.css';
import io from 'socket.io-client';

let socket;

function App() {
  socket = io('http://localhost:8080');
  const [value, setValue] = useState("");
  const [name, setName] = useState("");

  useEffect(() => {
    socket.on('socket', data => {
      console.log(data)
      setName(data)
    });
  }, []);
  
  useEffect(() => {
    socket.on('message', data => {
      console.log(data)
    });
    socket.on(value, data => {
      console.log(data)
    });
  });

  const onSubmitHandler = e => {
    e.preventDefault();
    socket.emit('message', value);
    setValue("");
  }

  return (
    <div className="App">
      <div className='App-header'>
        <p>Hello <span>{name}</span></p>
        <form onSubmit={onSubmitHandler}>
          <input value={value} onChange={e => setValue(e.target.value)} />
        </form>
      </div>
    </div>
  );
}

export default App;
