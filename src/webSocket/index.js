import React, { useEffect, useState } from 'react';
import './App.css';

const URL = 'ws://localhost:8080';

function App() {
  let ws = new WebSocket(URL);
  const [value, setValue] = useState("");
  const [name, setName] = useState("");

  useEffect(() => {
    ws.onopen = () => {
      console.log('connected');
    }
    ws.onmessage = e => {
      console.log('get message ->>', e.data);
      const data = JSON.parse(e.data);
      console.log(data.data);

      if (data.type) {
        setName(data.data);
      }
    }

    ws.onclose = () => {
      console.log('disconnected');
    }

  }, [ws.onmessage, ws.onopen, ws.onclose]);

  const onSubmitHandler = (e) => {
    e.preventDefault();
    ws.send(value);
    setValue("");
  }

  return (
    <div className="App">
      <div className='App-header'>
        <p>Hello <span>{name}</span></p>
        <form onSubmit={onSubmitHandler}>
          <input value={value} onChange={e => setValue(e.target.value)} />
        </form>
      </div>
    </div>
  );
}

export default App;
